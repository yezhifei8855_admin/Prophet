# Prophet（预言家）
[Visit GitHub project](https://github.com/Miluer-tcq/Prophet) | [Visit Gitee Project](https://gitee.com/t940783074/Prophet)

[中文](README.md) | [English](README.en.md)
## Voting prediction platform based on Ethereum blockchain smart contract technology and Vue framework
Under the condition of cost constraints, the overall voting results have a good reference value for predicting the future trend of events. At the same time, block chain technology is used to avoid the possibility of developers tampering with data, ensure the security and reliability of voting data, and enhance the credibility of the platform.
- ## [The project has been deployed, online Demo](https://t940783074.gitee.io/prophet)
Due to international network restrictions, it is not possible to directly use [Infura](https://infura.io/) to access the Ethereum Dapp, so the complete experience of the platform uses the solution of installing the [MetaMask](https://metamask.io/) browser plug-in.
## Function and optimization
- [x] Connect blockchain
- [x] Create user
- [x] Personal information display
- [x] Voting event display
- [x] Create event
- [x] Event voting
- [x] Event reporting
- [x] Apply for rewards
- [x] Different colors show event status
- [x] Show user all operation records
- [x] Other

## Smart contract data structure
```
//完整代码见 src/Solidity

contract Ballot{
    struct Event{
        uint ID;//事件ID
        address Sponsor;//发起人 
        string Content;//事件内容 
        string[] Option;//事件选项 
        address Reporter;//汇报人 
        uint StartDate;//开始时间 
        uint Deadline;//截至时间 
        address [] Voters;//投票人 
        uint PrizePool;//奖池 
        bool Report;//汇报状态 默认为false
        uint Result;//事件结果 对应选项索引 默认为99
    }
    
    struct Player{
        address Uesr;//用户地址 
        uint Balance;//用户余额 
        uint[] Created;//已创建ID
        uint[] Selected;//已投票ID
        uint[] Report;//需汇报ID
        mapping(uint=>Option) OptionInfo;//每个已投票事件的选项资金 
    }
    
    struct Option{
        bool Voted;//事件投票状态 
        uint Time; //投票时间
        uint Option;//事件选项 
        uint Finance;//事件金额 
        bool Reward;//事件奖励状态  
    }
    
    struct Finance{
        mapping(uint=>uint) Finance;//选项对应金额 
    }
    
    struct Log{
        uint Time;
        uint ID;
        string action;
        int Change;
        uint Balance;
    }
        

    Event[] Events;//总事件
    mapping(uint=>Finance) EventFinance;//事件对应金额 
    address[] ExistPlayers;//总用户 
    mapping(address=>Player) Players;//用户对应信息 
    mapping(address=>Log[]) PlayerLogs;
```
## Technology used
- [Ethereum](https://ethereum.org/zh/)
- [Solidity](https://solidity.readthedocs.io/en/v0.6.10/)
- [Web3.js](https://github.com/ethereum/web3.js/)
- [MetaMask](https://metamask.io/)
- VueCli 3.x
- Vue 2.x
- Vuex
- Vue Router
- [Element UI](http://element.eleme.io/#/zh-CN)
- ES6
- Node.js
- axios
- webpack
- Other

Note: In VueCli 2.x, public files are placed in the static folder, and in VueCli 3.x versions, please put them in the public folder, otherwise Axios cannot find the files. The project uses VueCli 3.x.

## Brief introduction of the project
- Classified display events
- Progress bars and colors represent events for different durations of validity
- Provide view details button
![](img/概览.png)
- Display event status, current voting information and all user voting records, etc.
![](img/事件详情信息.png)
- Classified display events
- Report event icon reminder
![](img/个人事件.png)
- form validation
- Quick date selection
![](img/创建事件.png)
- Create user
- Blockchain information display
![](img/简略个人信息.png)
- Eye-catching personal platform token balance
- Personal operation record
![](img/个人详情信息.png)
- Some small details（[Browse the demo to find out more](https://t940783074.gitee.io/prophet)）
![](img/交易.png)
![](img/交易1.png)


## Project setup
```
npm install
```

## Compiles and hot-reloads for development
```
npm run serve
```

## Compiles and minifies for production
```
npm run build
```

## Customize configuration
See [Configuration Reference](https://cli.vuejs.org/config/).

## Open source agreement
- Please follow the original author MIT open source agreement
